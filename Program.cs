﻿using System.Text;
using System.Text.RegularExpressions;

namespace VigenereCipher
{
    public class Program
    {
        public static void Main(string[] args)
        {
            bool userWishesToContinue = true;
            while (userWishesToContinue)
            {
                Console.WriteLine("--------------------\nVigenère Cipher\n--------------------");
                Console.Write("1) Encrypt\n2) Decrypt\n3) Quit\n--------------------\nYour Choice: ");
                string? choice = Console.ReadLine();
                if (choice == null)
                {
                    Console.Clear();
                    continue;
                }
                switch (choice[0])
                {
                    case '1':
                        EncryptHelper();
                        break;
                    case '2':
                        DecryptHelper();
                        break;
                    case '3':
                        userWishesToContinue = false;
                        break;
                }
                Console.Clear();
            }
        }

        public static void EncryptHelper()
        {
            Console.WriteLine("Enter the message to encrypt");
            string input = ReadAlphaString();
            Console.WriteLine("\nEnter the encryption key:");
            string key = ReadAlphaString(input.Length);
            string encrypted = Cipher.Encrypt(input.ToLower(), key.ToLower());
            Console.WriteLine($"The encrypted string is {encrypted}");
            Console.WriteLine("Press enter to continue.");
            Console.ReadLine();
        }

        public static void DecryptHelper()
        {
            Console.WriteLine("Enter the message to decrrypt");
            string input = ReadAlphaString();
            Console.WriteLine("\nEnter the encryption key:");
            string key = ReadAlphaString(input.Length);
            string decrypted = Cipher.Decrypt(input.ToLower(), key.ToLower());
            Console.WriteLine($"The encrypted string is {decrypted}");
            Console.WriteLine("Press enter to continue.");
            Console.ReadLine();
        }

        private static string ReadAlphaString()
        {
            string? input;
            do
            {
                Console.Write("Please enter a string of letters: ");
                input = Console.ReadLine();
            } while (string.IsNullOrWhiteSpace(input) || !IsAlpha(input));

            return input;
        }

        private static string ReadAlphaString(int length)
        {
            string? input;
            do
            {
                Console.Write($"Please enter a string of {length} letters: ");
                input = Console.ReadLine();
            } while (string.IsNullOrWhiteSpace(input) || input.Length != length || !IsAlpha(input));

            return input;
        }

        private static bool IsAlpha(string str)
        {
            Regex rg = new Regex("^[a-zA-Z]+$");
            return rg.IsMatch(str);
        }


    }   
}