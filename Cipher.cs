﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VigenereCipher
{
    public static class Cipher
    {
        public static string Encrypt(string input, string key)
        {
            //input and key must both be lowercase
            StringBuilder sb = new StringBuilder(input.Length);
            //follow Vigenere Cipher Algorithm
            for (int i = 0; i < input.Length; i++)
            {
                int offsetInput = input[i] - 'a';
                int offsetKey = key[i] - 'a';
                int offsetCipher = (offsetInput + offsetKey) % 26;
                char c = (char)('a' + offsetCipher);

                sb.Append(c);
            }
            return sb.ToString();
        }

        public static string Decrypt(string input, string key)
        {
            //input and key must both be lowercase
            StringBuilder sb = new StringBuilder(input.Length);

            for (int i = 0; i < input.Length; i++)
            {
                int offsetInput = input[i] - 'a';
                int offsetKey = key[i] - 'a';
                int offsetCipher = (offsetInput - offsetKey) % 26;
                if (offsetCipher < 0)
                    offsetCipher = 26 + offsetCipher;
                char c = (char)('a' + offsetCipher);

                sb.Append(c);
            }
            return sb.ToString();
        }
    }
}
